# Docker/Singularity container for R environment with htSeqTools

## Specifications
- `R` 3.4.0 with `X11`
- `Bioc` 3.6
- `BiocInstaller` 1.28.0
- `htSeqTools` 1.26.0

## Use of the container: 

```
# to build the Docker
make build

# to build the Singularity 
make build-sif

# run the shell to get inside the docker container
make shell

# example running a script via docker
make run-r

# example running a script via singularity
make run-r-sif
```
The created files will be in the sub-folder `output` of the current folder.

In reality when actively developing a script, we will want to mount it to a countainer instead of shipping with the container as in the example, as such (from inside the repository): 
### Docker
```
MOUNT_DIR=/work
docker run -e MOUNT_DIR=${MOUNT_DIR} -v ${PWD}:${MOUNT_DIR} -u $(UID):$(shell id -g) ${IMAGE_NAME} Rscript ${MOUNT_DIR}/src/analysis2.R
```
where `${IMAGE_NAME}` will look something like `local/pgt-tools:your-branch`. Check with `docker image ls` if in doubt.

### Singularity
```
export SINGULARITYENV_MOUNT_DIR=/work
singularity exec --cleanenv -B ${PWD}:${MOUNT_DIR} pgt-tools.sif ${MOUNT_DIR}/src/analysis2.R
```
The script `analysis2.R` is in active development and is not inside the container, but mounted into it. Any other new script `analysis3.R` will need to be placed inside `pgt-tools/src` and then it can be run in exact same way.

### Input data
To read in the external input files, make a separate bind, for example in Singularity:
```
singularity exec --cleanenv -B ${PWD}:/work -B /path/to/data:/data pgt-tools.sif /work/src/analysis2.R
```
Then in `analysis2.R` you will have something like this:

```
data_dir <- "/data"
list.files(data_dir)
input_filename <- file.path(data_dir, "youfilename.txt")
```

## Suggested development flow
 - use docker container with `make shell` to get access to R environment with needed packages and shape your scripts using editor of choice. 

 - test script automation with docker/Rscript
 - run with Singularity 