SHELL = /bin/bash

#----
# Set up
#----
MOUNT_DIR = /work
export SINGULARITYENV_MOUNT_DIR=${MOUNT_DIR}
BRANCH = $(shell git rev-parse --abbrev-ref HEAD)
IMAGE_NAME = local/pgt-tools:${BRANCH}

#----
# Build image
#----
build:
	docker build . -t ${IMAGE_NAME}
#----
# Build Singularity image
#----
build-sif: build
	singularity build pgt-tools.sif docker-daemon://${IMAGE_NAME}

#----
# Run image
#----
shell:
	docker run -v ${CURDIR}:${MOUNT_DIR} -it ${IMAGE_NAME} /bin/bash

#----
# Run analysis via Docker
#----
run-r:
	mkdir -p output
	docker run -e MOUNT_DIR=${MOUNT_DIR} \
		-v ${CURDIR}:${MOUNT_DIR} \
		-u ${UID}:$(shell id -g) \
		${IMAGE_NAME}

#----
# Run analysis via Singularity 
#----
run-r-sif:
	mkdir -p output
	 singularity exec --cleanenv \
	 	-B ${CURDIR}:${MOUNT_DIR} \
	 	pgt-tools.sif Rscript ${MOUNT_DIR}/src/analysis1.R
