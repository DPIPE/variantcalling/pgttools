FROM centos:centos7.9.2009 as base

ENV TERM xterm-256color

RUN yum -y update &&  \
    yum -y install \
       which \
       make \
       gcc \
       wget \
       tar \
       java-11-openjdk-devel \
       zlib-devel \
       bzip2-devel \
       libxml2-devel \
       libcurl-devel \
       openssl-devel \ 
       libicu-devel \
       libX11-devel \
       libXt-devel \
       cairo-devel \
       pango-devel \
       pango \
       libpng-devel \
       libtiff-devel \
       libjpeg-devel \
       glibc-locale-source && \
    yum -y reinstall \
        glibc-common && \
    yum -y groupinstall \
        "Development Tools"

RUN localedef -i en_US -f UTF-8 en_US.UTF-8 && \
       echo "LANG=en_US.UTF-8" > /etc/locale.conf

ENV LANG en_US.UTF-8

RUN wget https://cran.r-project.org/src/base/R-3/R-3.4.0.tar.gz && \
        tar xvzf R-3.4.0.tar.gz && \
        cd R-3.4.0 && \
        ./configure --with-x=yes --with-readline=no --build=aarch64-unknown-linux-gnu && \
        make && \
        make install && \
        cd ..

RUN  Rscript --vanilla -e 'options(download.file.method="wget"); source("http://bioconductor.org/biocLite.R"); biocLite("htSeqTools")'

ENV PGT_SRC=/opt/pgt-tools
COPY . ${PGT_SRC}
WORKDIR /work

CMD [ "Rscript", "/opt/pgt-tools/src/analysis1.R" ]